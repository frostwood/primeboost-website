import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Container from "@material-ui/core/Container";
import Grid from "@material-ui/core/Grid";
import Box from "@material-ui/core/Box";
import Typography from "@material-ui/core/Typography";
import product from "../../assets/offer-min.jpg";
import tokopedia from "../../assets/tokoButtonShadowed-min.png";
import shopee from "../../assets/shopeeButtonShadowed-min.png";
import whatsapp from "../../assets/waButtonShadowed-min.png";

const useStyles = makeStyles((theme) => ({
  root: {
    [theme.breakpoints.up("md")]: {
      paddingTop: theme.spacing(6),
      paddingBottom: theme.spacing(5),
    },
    [theme.breakpoints.down("sm")]: {
      paddingTop: theme.spacing(4),
      paddingBottom: theme.spacing(3),
    },
    backgroundColor: "#b53636",
    paddingBottom: theme.spacing(0),
  },
  heading: {
    [theme.breakpoints.up("md")]: {
      fontSize: "2.25rem",
      paddingBottom: theme.spacing(2),
    },
    [theme.breakpoints.down("sm")]: {
      fontSize: "1.8rem",
      paddingBottom: theme.spacing(1),
    },
    color: "white",
  },
  contentWrapper: {
    [theme.breakpoints.up("md")]: {
      padding: theme.spacing(1),
    },
    [theme.breakpoints.down("sm")]: {
      padding: theme.spacing(1),
    },
  },
  image: {
    [theme.breakpoints.up("md")]: {
      width: "360px",
    },
    [theme.breakpoints.down("sm")]: {
      width: "350px",
    },
    [theme.breakpoints.down("xs")]: {
      width: "90%",
    },
  },
  buttonWrapper: {
    position: "relative",
  },
  button: {
    [theme.breakpoints.up("md")]: {
      width: "250px",
    },
    [theme.breakpoints.down("sm")]: {
      width: "200px",
    },
    [theme.breakpoints.down("xs")]: {
      width: "200px",
    },
  },
}));

export default function Products() {
  const classes = useStyles();

  const options = [
    {
      title: "Whatsapp",
      link: "https://wa.me/6285159931671",
      image: whatsapp,
    },
    {
      title: "Tokopedia",
      link: "https://www.tokopedia.com/primeboost",
      image: tokopedia,
    },
    {
      title: "Shopee",
      link: "https://shopee.co.id/prime.boost",
      image: shopee,
    },
  ];

  return (
    <div className={classes.root}>
      <Container maxWidth="md">
        <Grid container direction="column" justify="center" alignItems="center">
          <Grid item justify="center" alignItems="center" container>
            <Grid
              className={classes.contentWrapper}
              item
              md={6}
              sm={12}
              direction="column"
              align="center"
            >
              <Box borderRadius="8%" borderColor="#6b1313" border={8} clone>
                <img src={product} className={classes.image} alt="Primeboost" />
              </Box>
            </Grid>
            <Grid
              className={classes.contentWrapper}
              item
              md={6}
              sm={12}
              direction="column"
              alignItems="center"
              justify="center"
              container
            >
              <Grid item>
                <Typography
                  className={classes.heading}
                  variant="h1"
                  align="center"
                >
                  Boost Your Health
                </Typography>
              </Grid>
              <Grid container
              md={6}
              sm={12}
              direction="Column"
              alignItems="center"
              justify="center">
              {options.map((item, index) => (
                <Grid
                  key={index}
                  className={classes.contentWrapper}
                  item
                  justify="center"
                  align="center"
                  elevation={3}
                >
                  <a key={index} href={item.link}  target="_blank" rel="noreferrer">
                    <img
                      src={item.image}
                      title={item.title}
                      alt={item.title}
                      className={classes.button}
                    />
                  </a>
                </Grid>
              ))}
              </Grid>

            </Grid>
          </Grid>
        </Grid>
      </Container>
    </div>
  );
}
