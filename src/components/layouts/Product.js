import React from "react";
import { makeStyles } from "@material-ui/core/styles";

import Container from "@material-ui/core/Container";
import Grid from "@material-ui/core/Grid";
import product from "../../assets/primeboost-min.jpg";
import Box from "@material-ui/core/Box";
import Typography from "@material-ui/core/Typography";

const useStyles = makeStyles((theme) => ({
  root: {
    [theme.breakpoints.up("md")]: {
      paddingTop: theme.spacing(5),
    },
    [theme.breakpoints.down("sm")]: {
      paddingTop: theme.spacing(3),
    },
    backgroundColor: "#b53636",
    paddingBottom: theme.spacing(0),
  },
  heading: {
    [theme.breakpoints.up("md")]: {
      fontSize: "2.25rem",
      paddingBottom: theme.spacing(2),
    },
    [theme.breakpoints.down("sm")]: {
      fontSize: "1.8rem",
      paddingBottom: theme.spacing(1),
    },
    color: "white",
  },
  contentWrapper: {
    [theme.breakpoints.up("md")]: {
      padding: theme.spacing(3),
    },
    [theme.breakpoints.down("sm")]: {
      padding: theme.spacing(2),
    },
  },
  paragraph: {
    paddingTop: theme.spacing(3),
    [theme.breakpoints.up("md")]: {
      fontSize: "1.2rem",
    },
    [theme.breakpoints.down("sm")]: {
      fontSize: "0.9rem",
    },
    color: "white",
  },
  image: {
    [theme.breakpoints.up("md")]: {
      width: "360px",
    },
    [theme.breakpoints.down("sm")]: {
      width: "350px",
    },
    [theme.breakpoints.down("xs")]: {
      width: "90%",
    },
  },
}));

export default function Products() {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <Container maxWidth="md">
        <Grid container direction="column" justify="center" alignItems="center">
          <Grid item>
            <Typography className={classes.heading} variant="h1" align="center">
              Apa Solusinya?
            </Typography>
          </Grid>
          <Grid item justify="center" alignItems="center" container>
            <Grid className={classes.contentWrapper} item md={6} sm={12}>
              <Typography
                className={classes.paragraph}
                variant="h6"
                align="justify"
              >
                <b>
                  <i>PrimeBoost</i>
                </b>{" "}
                adalah minuman suplemen herbal yang memiliki kandungan bahan
                alami seperti Astaxanthin, White Ginseng, Cordyceps, Red Beet,
                dan Red Grape. Perpaduan bahan alami tersebut memiliki khasiat
                yang sangat efektif untuk membersihkan dan melancarkan pembuluh
                darah serta menjaga stamina dan sistem kekebalan tubuh agar
                tetap fit dan aktif setiap hari.
              </Typography>
            </Grid>
            <Grid
              className={classes.contentWrapper}
              item
              md={6}
              sm={12}
              direction="column"
              align="center"
            >
              <Box borderRadius="8%" borderColor="#6b1313" border={8} clone>
                <img src={product} className={classes.image} alt="Primeboost" />
              </Box>
            </Grid>
          </Grid>
        </Grid>
      </Container>
    </div>
  );
}
