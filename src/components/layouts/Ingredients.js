import React from "react";
import { makeStyles } from "@material-ui/core/styles";

import anggur from "../../assets/anggur-merah.jpg";
import astaxanthin from "../../assets/astaxanthine-min.png";
import beetroot from "../../assets/bit-merah.jpg";
import ginseng from "../../assets/white-ginseng-min.jpg";
import cordyceps from "../../assets/cordyceps-min.jpg";
import Container from "@material-ui/core/Container";
import Grid from "@material-ui/core/Grid";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import CardMedia from "@material-ui/core/CardMedia";
import Typography from "@material-ui/core/Typography";
import Paper from "@material-ui/core/Paper";

const useStyles = makeStyles((theme) => ({
  root: {
    [theme.breakpoints.up("md")]: {
      paddingTop: theme.spacing(5),
      paddingBottom: theme.spacing(4),
    },
    [theme.breakpoints.down("sm")]: {
      paddingTop: theme.spacing(3),
      paddingBottom: theme.spacing(2),
    },
  },
  heading: {
    [theme.breakpoints.up("md")]: {
      fontSize: "2.25rem",
      paddingBottom: theme.spacing(2),
    },
    [theme.breakpoints.down("sm")]: {
      fontSize: "1.8rem",
      paddingBottom: theme.spacing(1),
    },
    color: "#b53636 ",
  },
  cardWrapper: {
    margin: theme.spacing(3),
  },
  cardRoot: {
    maxWidth: 380,
    color: "white",
    backgroundColor: "#b53636",
    [theme.breakpoints.up("md")]: {
      height: 430,
    },
    [theme.breakpoints.down("sm")]: {
      height: 420,
    },
    [theme.breakpoints.down("xs")]: {
      height: 435,
    },
  },
  cardTitle: {
    [theme.breakpoints.up("md")]: {
      fontSize: "1.5rem",
    },
    [theme.breakpoints.down("sm")]: {
      fontSize: "1.3rem",
    },
    color: "white",
  },
  cardDesc: {
    [theme.breakpoints.up("md")]: {
      fontSize: "0.9rem",
    },
    [theme.breakpoints.down("sm")]: {
      fontSize: "0.8rem",
    },
    color: "white",
  },
  media: {
    height: 275,
  },
}));

export default function Ingredients() {
  const classes = useStyles();

  const cardItems = [
    {
      name: "Astaxanthin",
      description:
        "Antioksidan alami yang ampuh dalam " +
        "memerangi radikal bebas dan Anti-Inflamatory melawan peradangan serta " +
        "menjaga keremajaan pembuluh darah dan persendian.",
      image: astaxanthin,
    },
    {
      name: "White Ginseng",
      description:
        "Meningkatkan sistem kekebalan tubuh, membantu menurunkan " +
        "kadar gula dalam darah serta membantu menekan tingkat stress, " +
        "melancarkan pernapasan dan sirkulasi darah.",
      image: ginseng,
    },
    {
      name: "Cordyceps",
      description:
        "Meningkatkan daya tahan tubuh dan stamina dalam beraktivitas, "+
        "memberikan perlindungan terhadap organ tubuh seperti jantung dan liver.",
      image: cordyceps,
    },
    {
      name: "Red Grape",
      description:
        "Menurunkan daya serap kolesterol jahat ke dalam tubuh dan " +
        "meningkatkan sensitivitas insulin membantu " +
        "mengelola kadar gula di dalam darah.",
      image: anggur,
    },
    {
      name: "Red Beet",
      description:
        "Kaya akan vitamin dan mineral yang baik dalam mengontrol tekanan "+
        "darah serta memaksimalkan penggunaan oksigen selama beraktivitas.",
      image: beetroot,
    },
  ];

  return (
    <div className={classes.root}>
      <Container maxWidth="md">
        <Grid container direction="column" justify="center" alignItems="center">
          <Grid item>
            <Typography className={classes.heading} variant="h1" align="center">
              Kandungan PrimeBoost
            </Typography>
          </Grid>
          <Grid item>
            <Grid container justify="center" alignItems="center">
              {cardItems.map((item, index) => (
                <Grid key={index} item>
                  <Paper className={classes.cardWrapper} elevation={5}>
                    <Card className={classes.cardRoot}>
                      <CardMedia
                        className={classes.media}
                        image={item.image}
                        title={item.name}
                      />
                      <CardContent>
                        <Typography
                          className={classes.cardTitle}
                          variant="h5"
                          gutterBottom
                        >
                          {item.name}
                        </Typography>
                        <Typography
                          className={classes.cardDesc}
                          variant="body1"
                          color="white"
                          align="justify"
                        >
                          {item.description}
                        </Typography>
                      </CardContent>
                    </Card>
                  </Paper>
                </Grid>
              ))}
            </Grid>
          </Grid>
        </Grid>
      </Container>
    </div>
  );
}
