import React from "react";
import { makeStyles } from "@material-ui/core/styles";

import Container from "@material-ui/core/Container";
import Grid from "@material-ui/core/Grid";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import CardMedia from "@material-ui/core/CardMedia";
import Typography from "@material-ui/core/Typography";
import Paper from "@material-ui/core/Paper";
import manfaat from "../../assets/manfaat-min.jpg";

const useStyles = makeStyles((theme) => ({
  root: {
    [theme.breakpoints.up("md")]: {
      paddingBottom: theme.spacing(0),
      paddingTop: theme.spacing(6),
    },
    [theme.breakpoints.down("sm")]: {
      paddingBottom: theme.spacing(0),
      paddingTop: theme.spacing(4),
    },
    [theme.breakpoints.down("xs")]: {
      paddingBottom: theme.spacing(0),
      paddingTop: theme.spacing(3),
    },
  },
  cardRoot: {
    [theme.breakpoints.up("md")]: {
      height: 350,
      display: "flex",
    },
    [theme.breakpoints.down("sm")]: {
      height: 675,
    },
    [theme.breakpoints.down("xs")]: {
      height: 650,
    },
    backgroundColor: "#b53636",
  },
  cardWrapper: {
    margin: theme.spacing(2),
  },
  contentWrapper: {
    display: "flex",
    alignItems: "center",
  },
  content: {
    display: "flex",
    flexDirection: "column",
    alignItems: "flex-start",
    [theme.breakpoints.up("md")]: {
      width: 500,
      paddingLeft: theme.spacing(5),
    },
    [theme.breakpoints.down("sm")]: {
      width: 400,
      paddingLeft: theme.spacing(4),
    },
    [theme.breakpoints.down("xs")]: {
      width: 300,
      paddingLeft: theme.spacing(2),
    },
  },
  cardTitle: {
    [theme.breakpoints.up("md")]: {
      fontSize: "2.2rem",
      paddingBottom: theme.spacing(2),
    },
    [theme.breakpoints.down("sm")]: {
      fontSize: "1.8rem",
      paddingBottom: theme.spacing(1),
    },
    [theme.breakpoints.down("xs")]: {
      fontSize: "1.5rem",
    },
    color: "white",
  },
  cardDesc: {
    [theme.breakpoints.up("md")]: {
      fontSize: 18,
      paddingBottom: theme.spacing(1.5),
    },
    [theme.breakpoints.down("sm")]: {
      fontSize: 14,
      paddingBottom: theme.spacing(1.5),
    },
    [theme.breakpoints.down("xs")]: {
      fontSize: 14,
      paddingBottom: theme.spacing(1.5),
    },
    color: "white",
  },
  cover: {
    [theme.breakpoints.up("md")]: {
      width: 350,
    },
    [theme.breakpoints.down("sm")]: {
      height: 400,
    },
    [theme.breakpoints.down("xs")]: {
      height: 300,
    },
  },
}));

export default function Ingredients() {
  const classes = useStyles();

  const usecase = [
    {
      usecase: "•	Menjaga kesehatan peredaran darah dan jantung",
    },
    {
      usecase: "•	Meningkatkan stamina dan sistem kekebalan tubuh",
    },
    {
      usecase: "•	Anti-Inflamatory untuk melawan peradangan",
    },
    {
      usecase: "•	Antioksidan tinggi membantu melawan radikal bebas",
    },
    {
      usecase: "•	Membantu menurunkan kadar gula darah berlebih",
    },
    {
      usecase: "•	Membantu melancarkan pernapasan",
    },
  ];

  return (
    <div className={classes.root}>
      <Container maxWidth="md">
        <Grid container justify="center" alignItems="center">
          <Paper className={classes.cardWrapper} elevation={3}>
            <Card className={classes.cardRoot} raised={true}>
              <CardMedia
                className={classes.cover}
                image={manfaat}
                title="PrimeBoost"
              />
              <div className={classes.contentWrapper}>
                <CardContent className={classes.content}>
                  <Typography
                    className={classes.cardTitle}
                    variant="h5"
                    gutterBottom
                  >
                    Manfaat PrimeBoost :
                  </Typography>
                  {usecase.map((item, index) => (
                    <Typography
                      key={index}
                      className={classes.cardDesc}
                      variant="body1"
                      color="white"
                    >
                      {item.usecase}
                    </Typography>
                  ))}
                </CardContent>
              </div>
            </Card>
          </Paper>
        </Grid>
      </Container>
    </div>
  );
}
