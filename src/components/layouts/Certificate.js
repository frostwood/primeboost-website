import React from "react";
import logoBPOM from "../../assets/logoBPOM-min-min.png";
import logoHalal from "../../assets/logoHalal-min-min.png";
import logoGMP from "../../assets/logoGMP-min-min.png";
import Typography from "@material-ui/core/Typography";
import { makeStyles } from "@material-ui/core/styles";
import Container from "@material-ui/core/Container";
import Grid from "@material-ui/core/Grid";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import CardMedia from "@material-ui/core/CardMedia";
import Paper from "@material-ui/core/Paper";

const useStyles = makeStyles((theme) => ({
  root: {
    [theme.breakpoints.up("md")]: {
      paddingBottom: theme.spacing(4),
    },
    [theme.breakpoints.down("sm")]: {
      paddingBottom: theme.spacing(3),
    },
    backgroundColor: "#b53636",
  },
  heading: {
    [theme.breakpoints.up("md")]: {
      fontSize: "2.25rem",
      paddingBottom: theme.spacing(2),
    },
    [theme.breakpoints.down("sm")]: {
      fontSize: "1.8rem",
      paddingBottom: theme.spacing(1),
    },
    color: "white ",
  },
  cardRoot: {
    display: "flex",
    [theme.breakpoints.up("md")]: {
      height: 75,
    },
    [theme.breakpoints.down("sm")]: {
      height: 60,
    },
  },
  cardWrapper: {
    margin: theme.spacing(2),
  },
  contentWrapper: {
    display: "flex",
    alignItems: "center",
  },
  content: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    justify: "space-between",
    align: "center",
    [theme.breakpoints.up("md")]: {
      width: 175,
    },
    [theme.breakpoints.down("sm")]: {
      width: 125,
    },
  },
  cardTitle: {
    [theme.breakpoints.up("md")]: {
      fontSize: "1.5rem",
    },
    [theme.breakpoints.down("sm")]: {
      fontSize: "1.1rem",
    },
  },
  cardDesc: {
    [theme.breakpoints.up("md")]: {
      fontSize: "0.9rem",
    },
    [theme.breakpoints.down("sm")]: {
      fontSize: "0.7rem",
    },
  },
  cover: {
    [theme.breakpoints.up("md")]: {
      width: 75,
    },
    [theme.breakpoints.down("sm")]: {
      width: 60,
    },
  },
}));

export default function Certificate() {
  const classes = useStyles();

  const cardItems = [
    {
      title: "Badan POM RI",
      description: "MD 867009111575",
      image: logoBPOM,
    },
    {
      title: "Halal",
      description: "Majelis Ulama Indonesia",
      image: logoHalal,
    },
    {
      title: "GMP",
      description: "Pabrik Bersertifikat",
      image: logoGMP,
    },
  ];

  return (
    <div className={classes.root}>
      <Container maxWidth="lg">
        <Grid
          container
          direction="column"
          justify="center"
          alignItems="center"
          display="flex"
          wrap
        >
          <Grid item>
            <Typography className={classes.heading} variant="h1" align="center">
              Sertifikasi Produk
            </Typography>
          </Grid>
          <Grid
            item
            container
            direction="row"
            justify="center"
            alignItems="center"
          >
            {cardItems.map((item, index) => (
              <Grid key={index} item>
                <div className={classes.cardWrapper}>
                  <Paper elevation={5}>
                    <Card className={classes.cardRoot} >
                      <CardMedia
                        className={classes.cover}
                        image={item.image}
                        title={item.title}
                      />
                      <div className={classes.contentWrapper}>
                        <CardContent className={classes.content}>
                          <Typography
                            className={classes.cardTitle}
                            variant="h5"
                          >
                            {item.title}
                          </Typography>
                          <Typography
                            className={classes.cardDesc}
                            variant="body2"
                            align="center"
                          >
                            {item.description}
                          </Typography>
                        </CardContent>
                      </div>
                    </Card>
                  </Paper>
                </div>
              </Grid>
            ))}
          </Grid>
        </Grid>
      </Container>
    </div>
  );
}
