import React from "react";
import { makeStyles } from "@material-ui/core/styles";

import Container from "@material-ui/core/Container";
import Grid from "@material-ui/core/Grid";
import Paper from "@material-ui/core/Paper";
import Typography from "@material-ui/core/Typography";
import Accordion from "@material-ui/core/Accordion";
import AccordionSummary from "@material-ui/core/AccordionSummary";
import AccordionDetails from "@material-ui/core/AccordionDetails";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";

const useStyles = makeStyles((theme) => ({
  root: {
    [theme.breakpoints.up("md")]: {
      paddingTop: theme.spacing(5),
      paddingBottom: theme.spacing(4),
    },
    [theme.breakpoints.down("sm")]: {
      paddingTop: theme.spacing(3),
      paddingBottom: theme.spacing(2),
    },
  },
  heading: {
    [theme.breakpoints.up("md")]: {
      fontSize: "2.25rem",
      paddingBottom: theme.spacing(2),
    },
    [theme.breakpoints.down("sm")]: {
      fontSize: "1.8rem",
      paddingBottom: theme.spacing(1),
    },
    color: "#b53636 ",
  },
  accordionHead: {
    [theme.breakpoints.up("md")]: {
      fontSize: "1.5rem",
    },
    [theme.breakpoints.down("sm")]: {
      fontSize: "1.3rem",
    },
    color: "white",
    backgroundColor: "#b53636",
  },
  accordionAnswer: {
    [theme.breakpoints.up("md")]: {
      fontSize: "1.5rem",
    },
    [theme.breakpoints.down("sm")]: {
      fontSize: "1.3rem",
    },
    color: "#b53636",

    backgroundColor: "white",
  },
  questionHead: {},
  questionAnswer: {},
}));

export default function Questions() {
  const classes = useStyles();

  const faqItems = [
    {
      question: "Apakah PrimeBoost halal untuk dikonsumsi?",
      answer:
        "PrimeBoost telah mendapatkan sertifikasi Halal dari MUI jadi jangan " +
        "khawatir tentang kandungan dalam PrimeBoost.",
      id: "faq1",
    },
    {
      question: "Dimana saya dapat memperoleh PrimeBoost?",
      answer:
        "Saat ini kami hanya ada di marketplace online seperti Shopee, " +
        'Tokopedia. Cukup search dengan kata kunci "PrimeBoost".',
      id: "faq2",
    },
    {
      question:
        "Apakah PrimeBoost aman dikonsumsi oleh anak kecil dan ibu hamil?",
      answer:
        "Kami tidak menyarankan anak kecil atau ibu hamil untuk " +
        "mengkonsumsi PrimeBoost.",
      id: "faq3",
    },
    {
      question: "Apakah khasiat dari PrimeBoost instan?",
      answer:
        "Khasiat/hasil tergantung kepada kondisi tubuh setiap orang, " +
        "jadi tidak bisa disamakan hasilnya.",
      id: "faq4",
    },
    {
      question: "Berapa kali sehari dosis konsumsi PrimeBoost?",
      answer:
        "Kami menyarankan untuk mengkonsumsi 1 sachet PrimeBoost setiap " +
        "hari setelah makan.",
      id: "faq5",
    },
    {
      question: "Apakah PrimeBoost menimbulkan efek ketergantungan?",
      answer: "PrimeBoost tidak menimbulkan efek ketergantungan.",
      id: "faq6",
    },
  ];

  return (
    <div className={classes.root}>
      <Container maxWidth="md">
        <Grid container direction="column" justify="center" alignItems="center">
          <Grid item>
            <Typography className={classes.heading} variant="h1" align="center">
              Pertanyaan Tentang PrimeBoost
            </Typography>
          </Grid>
          <Grid item>
            <Paper className={classes.cardWrapper} elevation={3}>
              {faqItems.map((item, index) => (
                <div key={index}>
                  <Accordion>
                    <AccordionSummary
                      expandIcon={<ExpandMoreIcon />}
                      aria-controls={item.id}
                      id={item.id}
                      className={classes.accordionHead}
                    >
                      <Typography className={classes.questionHead}>
                        {item.question}
                      </Typography>
                    </AccordionSummary>
                    <AccordionDetails className={classes.accordionAnswer}>
                      <Typography className={classes.questionAnswer}>
                        {item.answer}
                      </Typography>
                    </AccordionDetails>
                  </Accordion>
                </div>
              ))}
            </Paper>
          </Grid>
        </Grid>
      </Container>
    </div>
  );
}
