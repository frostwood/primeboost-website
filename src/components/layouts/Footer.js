import React from "react";
import Container from "@material-ui/core/Container";
import Typography from "@material-ui/core/Typography";
import Link from "@material-ui/core/Link";
import Box from "@material-ui/core/Box";
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({

  footerWrapper: {
  },
  footer: {
    right: "0",
    bottom: "0",
    left: "0",
    background: "#b53636",
    color: "white",
    borderTop: "1px solid ${theme.palette.divider}",
    marginTop: theme.spacing(8),
    paddingTop: theme.spacing(2),
    paddingBottom: theme.spacing(2),
  },
  footerLink: {
    fontSize: "15px",
    color: "inherit",
    variant: "subtitle1",
    "&:hover": {
      color: "inherit",
    },
  },
  copyright: {
    color: "white",
  },
  copyrightLink: {
    color: "inherit",
    "&:hover": {
      color: "inherit",
    },
  },
}));

function Copyright() {
  const classes = useStyles();
  return (
    <Typography variant="body2" className={classes.copyright} align="center">
    <Link className={classes.copyrightLink} href="#">
      PrimeBoost
    </Link>
      {" ©"} {new Date().getFullYear()}
    </Typography>
  );
}

export default function Footer() {
  const classes = useStyles();
  return (
    <React.Fragment>
    <div className={classes.footerWrapper}>
      <div className={classes.footer}>
        <Container lg={8}>
          <Typography variant="body2" align="center">
            Disclaimer : Hasil yang didapatkan setiap individu bisa
            berbeda-beda, semua itu tergantung dari kondisi tubuh dan
            metabolisme masing-masing.
          </Typography>
          <Box mt={2}>
            <Copyright />
          </Box>
        </Container>
      </div>
      </div>
    </React.Fragment>
  );
}
