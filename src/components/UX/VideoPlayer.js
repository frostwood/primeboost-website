import React, { Suspense } from "react";
import { makeStyles } from "@material-ui/core/styles";

import styled from "styled-components";
import ReactPlayer from "react-player";
import Paper from "@material-ui/core/Paper";
import Loading from "../UX/Loading";

const useStyles = makeStyles((theme) => ({
  playerWrapper: {
    position: "relative",
    [theme.breakpoints.up("md")]: {
      paddingTop: 120,
      paddingLeft: 200,
    },
    [theme.breakpoints.down("sm")]: {
      height: 300,
      maxWidth: 540,
    },
    [theme.breakpoints.down("xs")]: {
      height: 180,
      maxWidth: 330,
    },
  },
}));
//
// const StyledPaper = styled(Paper)`
//   & > div {
//     position: relative;
//     padding-top: 56.25%;
//   }
// `;

const StyledReactPlayer = styled(ReactPlayer)`
  & > div {
    position: absolute;
    top: 0;
    left: 0;
  }
`;

export default function VideoPlayer({ link, tumb }) {
  const classes = useStyles();
  return (
    <div>
      <Paper className={classes.playerWrapper} elevation={6}>
        <Suspense fallback={Loading}>
          <StyledReactPlayer url={link} />
        </Suspense>
      </Paper>
    </div>
  );
}
